package am.matveev.spring.OnlineShopping.services;

import am.matveev.spring.OnlineShopping.dto.OrdersDTO;
import am.matveev.spring.OnlineShopping.entity.OrdersEntity;
import am.matveev.spring.OnlineShopping.exceptions.OrdersNotFoundException;
import am.matveev.spring.OnlineShopping.mappers.OrdersMapper;
import am.matveev.spring.OnlineShopping.repositories.OrdersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrdersService{

    private final OrdersRepository ordersRepository;
    private final OrdersMapper ordersMapper;

    @Transactional(readOnly = true)
    public List<OrdersDTO> findAll(){
       List <OrdersEntity> orders = ordersRepository.findAll();
        List<OrdersDTO> ordersDTOS = orders.stream()
                .map(ordersMapper::toDTOWithProduct)
                .collect(Collectors.toList());
        return ordersDTOS;
    }

    @Transactional(readOnly = true)
    public OrdersDTO findOne(long id){
        OrdersEntity orders = ordersRepository.findById(id)
                .orElseThrow(OrdersNotFoundException ::new);
        return ordersMapper.toDTOWithProduct(orders);
    }

    @Transactional
    public OrdersDTO create(OrdersDTO ordersDTO){
        OrdersEntity orders = ordersMapper.toEntity(ordersDTO);
        ordersRepository.save(orders);
        return ordersMapper.toDTO(orders);
    }

    @Transactional
    public OrdersDTO update(long id , OrdersDTO updatedOrder){
        OrdersEntity orders = ordersMapper.toEntity(updatedOrder);
        orders.setId(id);
        ordersRepository.save(orders);
        return ordersMapper.toDTO(orders);
    }

    @Transactional
    public void delete(long id){
        ordersRepository.deleteById(id);
    }
}
