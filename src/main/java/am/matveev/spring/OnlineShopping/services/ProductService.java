package am.matveev.spring.OnlineShopping.services;

import am.matveev.spring.OnlineShopping.dto.ProductDTO;
import am.matveev.spring.OnlineShopping.entity.OrdersEntity;
import am.matveev.spring.OnlineShopping.entity.ProductEntity;
import am.matveev.spring.OnlineShopping.exceptions.OrdersNotFoundException;
import am.matveev.spring.OnlineShopping.exceptions.ProductNotFondException;
import am.matveev.spring.OnlineShopping.mappers.ProductMapper;
import am.matveev.spring.OnlineShopping.repositories.OrdersRepository;
import am.matveev.spring.OnlineShopping.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService{

    private final ProductRepository productRepository;
    private final OrdersRepository ordersRepository;
    private final ProductMapper productMapper;

    @Transactional(readOnly = true)
    public List<ProductDTO> findAll(){
        List<ProductEntity> products = productRepository.findAll();
        List<ProductDTO> productDTOS = products.stream()
                .map(productMapper::toDTO)
                .collect(Collectors.toList());
        return productDTOS;
    }

    @Transactional(readOnly = true)
    public ProductDTO findOne(long id){
        ProductEntity product = productRepository.findById(id)
                .orElseThrow(ProductNotFondException ::new);
        return productMapper.toDTO(product);
    }

    @Transactional
    public ProductDTO create(ProductDTO productDTO){
        ProductEntity product = productMapper.toEntity(productDTO);
        productRepository.save(product);
        return productDTO;
    }

    @Transactional
    public ProductDTO update(long id,ProductDTO updateProduct){
        ProductEntity product = productMapper.toEntity(updateProduct);
        product.setId(id);
        productRepository.save(product);
        return productMapper.toDTO(product);
    }

    @Transactional
    public void delete(long id){
        productRepository.deleteById(id);
    }

    @Transactional
    public ProductDTO addProductToOrder(long productId, long orderId){
        ProductEntity product = productRepository.findById(productId)
                .orElseThrow(ProductNotFondException::new);

        OrdersEntity orders = ordersRepository.findById(orderId)
                .orElseThrow(OrdersNotFoundException ::new);

        orders.getProducts().add(product);
        ordersRepository.save(orders);
        return productMapper.toDTO(product);
    }

}
