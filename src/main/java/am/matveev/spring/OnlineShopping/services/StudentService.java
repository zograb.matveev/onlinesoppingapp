package am.matveev.spring.OnlineShopping.services;

import am.matveev.spring.OnlineShopping.controllers.UniversityFeignClient;
import am.matveev.spring.OnlineShopping.dto.StudentDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService{

    private final UniversityFeignClient universityFeignClient;

    public List<StudentDTO> getAllStudentsFromUniversity() {
        return universityFeignClient.getAllStudents();
    }
}
