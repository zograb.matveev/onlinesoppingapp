package am.matveev.spring.OnlineShopping.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductDTO{

    @NotEmpty(message = "Product name should not be empty.")
    private String productName;

    @NotEmpty(message = "Description should not be empty.")
    private String description;

    @NotNull(message = "Price should not be empty.")
    @Min(value = 1,message = "Price should be greater then zero.")
    private BigDecimal price;

    @NotEmpty(message = "Currency should not be empty.")
    private String currency;
}
