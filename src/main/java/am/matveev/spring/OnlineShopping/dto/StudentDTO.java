package am.matveev.spring.OnlineShopping.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentDTO{

    @NotEmpty(message = "Name should not be empty")
    private String name;

    @NotEmpty(message = "Name should not be empty")
    private String surname;

    @NotNull(message = "Age should not be empty")
    private int age;

    @NotNull(message = "Date of admission should not be empty")
    private int dateOfAdmission;

}
