package am.matveev.spring.OnlineShopping.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class OrdersDTO{

    @NotEmpty(message = "Customer name should not be empty.")
    private String customerName;

    @NotEmpty(message = "Customer name should not be empty.")
    private String customerAddress;

    @Email(message = "Email should be valid.")
    private String customerEmail;

    @NotEmpty(message = "Customer name should not be empty.")
    private String orderStatus;

    @NotNull(message = "Date should not be empty.")
    private LocalDate orderDate;

    private List<ProductDTO> productDTOS;
}

