package am.matveev.spring.OnlineShopping.mappers;

import am.matveev.spring.OnlineShopping.dto.ProductDTO;
import am.matveev.spring.OnlineShopping.entity.ProductEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper{

    ProductDTO toDTO(ProductEntity product);
    ProductEntity toEntity(ProductDTO productDTO);
}
