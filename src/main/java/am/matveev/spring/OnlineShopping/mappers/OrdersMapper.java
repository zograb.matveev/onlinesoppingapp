package am.matveev.spring.OnlineShopping.mappers;

import am.matveev.spring.OnlineShopping.dto.OrdersDTO;
import am.matveev.spring.OnlineShopping.entity.OrdersEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OrdersMapper{

    OrdersDTO toDTO(OrdersEntity orders);
    OrdersEntity toEntity(OrdersDTO ordersDTO);

    @Mapping(target = "productDTOS",source = "products")
    OrdersDTO toDTOWithProduct(OrdersEntity orders);
}
