package am.matveev.spring.OnlineShopping.controllers;

import am.matveev.spring.OnlineShopping.dto.StudentDTO;
import am.matveev.spring.OnlineShopping.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/online-shopping")
public class OnlineShoppingController {

    private final UniversityFeignClient universityFeignClient;
    private final StudentService studentService;

    @GetMapping("/students")
    public List<StudentDTO> getAllStudents() {
        return studentService.getAllStudentsFromUniversity();
    }
}