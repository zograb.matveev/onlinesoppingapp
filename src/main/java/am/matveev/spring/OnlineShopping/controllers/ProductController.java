package am.matveev.spring.OnlineShopping.controllers;

import am.matveev.spring.OnlineShopping.dto.ProductDTO;
import am.matveev.spring.OnlineShopping.entity.ProductEntity;
import am.matveev.spring.OnlineShopping.services.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController{

    private final ProductService productService;

    @GetMapping()
    public List<ProductDTO> findAll(){
        return productService.findAll();
    }

    @GetMapping("/{id}")
    public ProductDTO findOne(@PathVariable long id){
        return productService.findOne(id);
    }

    @PostMapping()
    public ProductDTO create(@RequestBody @Valid ProductDTO productDTO){
        return productService.create(productDTO);
    }

    @PutMapping("/{id}")
    public ProductDTO update(@PathVariable long id,@RequestBody @Valid ProductDTO productDTO){
        return productService.update(id,productDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        productService.delete(id);
    }

    @PostMapping("/{productId}/addProduct/{orderId}")
    public ProductDTO addProductToOrder(@PathVariable long productId,
                                        @PathVariable long orderId){
        return productService.addProductToOrder(productId,orderId);
    }
}
