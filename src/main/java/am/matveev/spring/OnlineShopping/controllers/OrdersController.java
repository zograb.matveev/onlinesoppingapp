package am.matveev.spring.OnlineShopping.controllers;

import am.matveev.spring.OnlineShopping.dto.OrdersDTO;
import am.matveev.spring.OnlineShopping.services.OrdersService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/orders")
@RestController
@RequiredArgsConstructor
public class OrdersController{

    private final OrdersService ordersService;

    @GetMapping()
    public List<OrdersDTO> findAll(){
        return ordersService.findAll();
    }

    @GetMapping("/{id}")
    public OrdersDTO findOne(@PathVariable long id){
        return ordersService.findOne(id);
    }

    @PostMapping()
    public OrdersDTO create(@RequestBody @Valid OrdersDTO ordersDTO){
        return ordersService.create(ordersDTO);
    }

    @PutMapping("/{id}")
    public OrdersDTO update(@PathVariable long id, @RequestBody @Valid OrdersDTO ordersDTO){
        return ordersService.update(id,ordersDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        ordersService.delete(id);
    }
}
