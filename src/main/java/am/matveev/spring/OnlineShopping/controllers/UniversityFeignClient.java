package am.matveev.spring.OnlineShopping.controllers;

import am.matveev.spring.OnlineShopping.dto.StudentDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "university", url = "http://localhost:8081")
public interface UniversityFeignClient {

    @GetMapping("/students")
    List<StudentDTO> getAllStudents();
}
