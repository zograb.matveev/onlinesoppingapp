package am.matveev.spring.OnlineShopping.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler{

    @ExceptionHandler({OrdersNotFoundException.class})
    public ResponseEntity handleException(OrdersNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Order with this id wasn't found",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ProductNotFondException.class})
    public ResponseEntity handleException(ProductNotFondException e){
        ErrorResponse response = new ErrorResponse(
                "Product with this id wasn't found!",
                LocalDateTime.now()
        );
        return new ResponseEntity(response,HttpStatus.NOT_FOUND);
    }
}
