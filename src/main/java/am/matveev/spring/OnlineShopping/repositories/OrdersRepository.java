package am.matveev.spring.OnlineShopping.repositories;

import am.matveev.spring.OnlineShopping.entity.OrdersEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdersRepository extends JpaRepository<OrdersEntity,Long>{
}
