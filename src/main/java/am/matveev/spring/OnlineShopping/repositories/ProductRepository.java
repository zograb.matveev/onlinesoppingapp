package am.matveev.spring.OnlineShopping.repositories;

import am.matveev.spring.OnlineShopping.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity,Long>{
}
